Name:           python-retask
Version:        1.1.0
Release:        1
Summary:        Python module to create and manage distributed task queues
License:        MIT
URL:            http://retask.readthedocs.org/en/latest/index.html
Source0:        https://pypi.python.org/packages/source/r/retask/retask-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
BuildRequires:	python3-flit-core
BuildRequires:  python3-redis
BuildRequires:  python3-six


%global _description\
Python module to create and manage distributed task queues using redis.

%description %_description

%package -n python3-retask
Summary:        %{summary}
%{?python_provide:%python_provide python3-retask}
Requires:       python3-redis
Requires:       python3-six

%description -n python3-retask %_description

%prep
%setup -q -n retask-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-retask
%doc LICENSE
%{python3_sitelib}/retask*/

%changelog
* Thu Mar 14 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.1.0-1
- Update package to version 1.1.0

* Tue Feb 28 2023 lichaoran <pkwarcraft@hotmail.com> - 0.4-1
- Init package

